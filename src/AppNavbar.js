import './App.css'
import { useContext, Fragment, useState, useEffect } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from './UserContext';
import { add, total, list, quantity, remove, get } from 'cart-localstorage';
import Swal from 'sweetalert2';
import cartImage from './images/cart.png'


import Cart from './components/Cart'
import Checkout from './pages/Checkout'

import { Table, Button, Modal, Form } from 'react-bootstrap';

export default function AppNavbar(){


	// States to open/close modals
	const [showCart, setShowCart] = useState(false);

	// Functions to toggle the opening and closing of the "Cart" modal
	const openCart = () => setShowCart(true);
	const closeCart = () => setShowCart(false);

	
	const {user} = useContext(UserContext);
	console.log(user);



	const [rerender, setRerender] = useState(false);

	const [ isCartEmpty, setIsCartEmpty ] = useState(false);

	useEffect((rerender) => {
		console.log(list());
		if( list().length === 0){
			setIsCartEmpty(true);	
			setRerender(!rerender);
		}
		else{
			setIsCartEmpty(false);
			setRerender(!rerender);
		}
		
	}, [list()]);

	return(
			<Fragment>
			<Navbar className="navBar" expand="lg">
				<Navbar.Brand className="copperPlate" as={ Link } to='/'>JAG FS</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav" className = "d-flex justify-content-end">
					<Nav className="ml-auto">
						<Nav.Link as={ NavLink } to='/' exact>Home</Nav.Link>
						<Nav.Link as={ NavLink } to='/products' exact> Products</Nav.Link>

						{(user.id !== null) ?
							<Fragment>
							<Nav.Link as={ NavLink } to='/logout' exact> Logout</Nav.Link>
							</Fragment>
						:
							<Fragment>
								<Nav.Link as={ NavLink } to='/login' exact> Login</Nav.Link>
								<Nav.Link as={ NavLink } to='/register' exact> Register</Nav.Link>
							</Fragment>
						}
					</Nav>
					<Nav className="mr-auto">
						{(user.isAdmin === false) ?
							<Button className="cartBtnBg"  onClick={() => openCart()}><img className="cartBtn" src={cartImage}/></Button>
							:
							''
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>

			{/*// Cart Modal*/}
			<Modal show={showCart} onHide={closeCart} size="lg">
						<Form>
							<Modal.Header closeButton>
								<Modal.Title>Cart</Modal.Title>
							</Modal.Header>
							<Modal.Body>	
								<Cart />
							</Modal.Body>
							<Modal.Footer>
								<Button variant="secondary" onClick={closeCart}>Continue Shopping</Button>
							</Modal.Footer>
						</Form>
			</Modal>
			</Fragment>
		)
}
