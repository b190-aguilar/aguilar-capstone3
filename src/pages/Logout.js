import { Navigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';
import UserContext from '../UserContext';


export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();


	// sets the user state back to its original state.
	useEffect(() => {
		setUser({id:null})
	});

	return(
		<Navigate to='/login' />
	)
}
