import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	const data = {
		title: 'JAG Funko Store',
		content: 'Satisfying your Funko Needs',
		destination: '/products',
		label: 'Buy Now!'
	};

	return(
		<Fragment>
			<Banner data={data} />
			<Highlights />
		</Fragment>
	)
}
