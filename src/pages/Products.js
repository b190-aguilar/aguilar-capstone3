import { Fragment, useEffect, useState, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';


// import ProductCard from '../components/ProductCard.js'
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';


export default function Products() {

	const { user } = useContext(UserContext);
	const [ products, setProducts ] = useState([]);

	const fetchData = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			method: "GET",
			headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=> {
			console.log(data);

			setProducts(data);
		})

    }
	useEffect(() => {
        fetchData();
    }, []);



		return (
			<main classaName ="block col-2">
			<h2>Products</h2>
			<Fragment>
			<Container fluid>
			<Row>
				 {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
            {(user.isAdmin === true)
                ? <AdminView productsData={products} fetchData={fetchData}/>
                : <UserView productsData={products}/>
            }
            </Row>
            </Container>
			</Fragment>
			</main>
		)
}