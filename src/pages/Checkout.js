import { Col, Table, Button, Container } from 'react-bootstrap';
import { total, list, destroy } from 'cart-localstorage';
import { useNavigate } from 'react-router-dom';

import { useState, useEffect, Fragment } from 'react';
import Swal from 'sweetalert2';


export default function Checkout(e){
	const [products, setProducts] = useState([]);

	const [count, setCount] = useState(false);
	
	const navigate = useNavigate();
	const navigateProducts = () => {
		destroy();
		navigate('/products');
	}

	useEffect(() => {

			const productsArr = list().map(product => {
					// console.log(product)

				return(

					<tr key={product.id}>
						<td>{product.name}</td>
						<td>{product.price}</td>
						<td>{product.quantity}</td>
					</tr>

				)

			});
			setProducts(productsArr);

		}, [count]);

	

	// fetch('http://localhost:4000/orders/checkout', {
	// 	method: 'POST',
	// 	headers:{
	// 		Authorization: `Bearer ${localStorage.getItem('token')}`,
	// 		'Content-type': 'application/json'
	// 	},
	// 	body: JSON.stringify({
	// 		products: cart,
	// 		total: total()
	// 	})
	// })
	// .then(res => res.json())
	// .then(data => {
	// 	console.log(data);

	// 	if(data){
	// 		Swal.fire({
	// 			title: "Checkout Complete!",
	// 			icon:"success",
	// 		}).then(() =>{
	// 				navigate('/products');
	// 			});
	// 	}
	// 	else{
	// 		Swal.fire({
	// 			title: "Error",
	// 			icon: "error",
	// 			text:"Something went wrong. Please try again."
	// 		}).then(() =>{
	// 				navigate('/products');
	// 			});
	// 	}
	// });


	return (
		<main classaName ="block col-2">
		<h2>Checkout: Receipt</h2>
		<Fragment>
			<Container>
			<Col>
				<h2>Items</h2>
					<Table className="bgGray" striped bordered hover responsive>
						<thead className="bg-dark text-white">
							<tr>
								<th>Name</th>
								<th>Price</th>
								<th>Quantity</th>
							</tr>					
						</thead>
						<tbody>
						{products}
						</tbody>
				</Table>
				<h3>Total</h3>
				<h2>Php {total()}</h2>
			</Col>

			<Button 
						variant="primary" 
						onClick={navigateProducts}>
							Continue Shopping
			</Button>
		</Container>
		</Fragment>
		</main>
	)
}

