import './App.css';

// UserContext 
import { UserProvider } from './UserContext';

// when the dev wants to render multiple components inside a reuseable function such as App, Fragment component of react is used to enclosed these components to avoid errors in our frontend application; the pair of <> and </> can also be used
import { useEffect, useState } from 'react';
import { Container, Col, Row } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';


// Web Components
import AppNavbar from './AppNavbar';
import Cart from './components/Cart';
// import AdminView from "./components/AdminView";
import ProductView from "./components/ProductView";
// import UserView from "./components/UserView";

// Web Pages

import Home from "./pages/Home";
import Products from "./pages/Products";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Checkout from "./pages/Checkout";


function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

  fetch(`${ process.env.REACT_APP_API_URL }/users/profile`, {
    headers: {
      Authorization: `Bearer ${ localStorage.getItem('token') }`
    }
  })
  .then(res => res.json())
  .then(data => {

    // Set the user states values with the user details upon successful login.
    if (typeof data._id !== "undefined") {

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });

    // Else set the user states to the initial values
    } else {

      setUser({
        id: null,
        isAdmin: null
      });

    }

  })

  }, []);


  return (
    <div>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar /> 
          <Row className ="row">
            <Col>
              <Container fluid>
              <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/products' element={<Products />} />
                <Route path='/products/:productId' element={<ProductView />} />
                <Route path='/login' element={<Login />} />
                <Route path='/logout' element={<Logout />} />
                <Route path='/register' element={<Register />} />
                <Route path='*' element={<Error />} />
                <Route path='/cart' element={<Cart />} />
                <Route path='/checkout' element={<Checkout />} />
              </Routes>
              </Container>
            </Col>
          </Row>
      </Router>
    </UserProvider>
    </div>
  );
}


export default App;
