import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';


export default function ProductCard({productProp}) {

    console.log(productProp);    
    console.log(typeof productProp);

    const navigate = useNavigate();

    const { name, funkoNumber, description, isActive, price, stock, image, createdOn, _id } = productProp;



    return (

            
                <Col className="my-2" xs={12} md={3} lg={4}>
                    <Card className="bgGray">
                        <Card.Body>
                            <Card.Title>{name} #{funkoNumber}</Card.Title>

                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>

                            <Button className="btnStd" onClick={() => navigate(`/products/${_id}`)}>Details</Button>
                        </Card.Body>
                    </Card>
                </Col>

    )
}

