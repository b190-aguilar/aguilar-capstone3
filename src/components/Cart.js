import { Row, Col, Table, Button } from 'react-bootstrap';
import { total, list, quantity, remove, destroy } from 'cart-localstorage';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect, Fragment } from 'react';
import Swal from 'sweetalert2';

// import Checkout from '../pages/Checkout';

export default function Cart() {

	const navigate = useNavigate();

	// States to open/close modals
	const [showCart, setShowCart] = useState(false);

	// Functions to toggle the opening and closing of the "Cart" modal
	const closeCart = () => setShowCart(false);


	const [products, setProducts] = useState([]);

	const [count, setCount] = useState(false);


	useEffect(() => {

		const addItem = (product,qty) => {
			// console.log(product);
			if(product.quantity < product.stock ){
				quantity(product.id,qty);
				setCount(!count);
			}
			else{
				Swal.fire({
					title: "Add item failed",
					icon: "error",
					text:"Not enough Stock."
				});
				setCount(!count);
			}

			// setRerender(!rerender);		
		}	

		const reduceItem = (productId,qty) => {
			quantity(productId,qty);
			setCount(!count);
			// setRerender(!rerender);		
		}

		const removeItem = (productId) => {
			remove(productId);
			if(list().length === 0){
				destroy()
			}
			setCount(!count);
			// setRerender(!rerender);		
		}


		const productsArr = list().map(product => {
				// console.log(product)

			return(

				<tr key={product.id}>
					<td>{product.name}</td>
					<td>{product.price}</td>
					<td>{product.quantity}</td>
					<td>
						<Button variant="success" size="sm" onClick={() => addItem(product,1)}>Add</Button>
							&nbsp;
							{(product.quantity > 1) ?
								<Button variant ="danger" size="sm" onClick={() => reduceItem(product.id,-1)}>Reduce</Button>
								:
								<Button variant ="danger" size="sm" onClick={() => removeItem(product.id)}>Remove</Button>
							}
					</td>
				</tr>

			)

		});
		setProducts(productsArr);

	}, [count]);

	// console.log(list());
	// console.log(cart)


	function checkout(e) {
		e.preventDefault();	

		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
		method: 'POST',
		headers:{
			Authorization: `Bearer ${localStorage.getItem('token')}`,
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			products: list(),
			total: total()
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		if(data){
			Swal.fire({
				title: "Checkout Complete!",
				icon:"success",
			}).then(function(){
				setShowCart(false);	
				navigate('/checkout');

			})
		}
		else{
			Swal.fire({
				title: "Error",
				icon: "error",
				text:"Something went wrong. Please try again."
			})
		}
	});

	};

	return(
		<Fragment>
		<Row> 
			<Col>
				<h2>Items</h2>
					<Table striped bordered hover responsive>
						<thead className="bg-dark text-white">
							<tr>
								<th>Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Action</th>
							</tr>					
						</thead>
						<tbody>
						{products}
						</tbody>
				</Table>
				<h3>Total</h3>
				<h4>Php {total()}</h4>
			</Col>
		</Row>
		<Row>
			<Col xs={12} md={{span:4, offset:10}}>
				{(list().length === 0)?
				<Button variant="warning" disabled type="submit">Check Out</Button>
				:
				<Button variant="warning" type="submit" onClick={(e) => checkout(e)}>Check Out</Button>
				}
			</Col>
		</Row>
		</Fragment>
		);
}