import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){

	const { title, content, destination, label } = data;

	return(
		<Row>
			<Col className ="p-2 text-center">
				<h2>{title}</h2>
				<p>{content}</p>
				<Link className="buyNow" to ={destination}>{label}</Link>
			</Col>
		</Row>
		)
}