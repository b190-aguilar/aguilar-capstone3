import { useState, useEffect, useContext, Fragment } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { add, total, list } from 'cart-localstorage';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';


export default function ProductView(){

	const { user } = useContext(UserContext);

	

	const { productId } = useParams();

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ funkoNumber, setFunkoNumber ] = useState('');
	const [ stock, setStock ] = useState('');
	const [ image, setImage ] = useState('');

	const navigate = useNavigate();
	const navigateProducts = () => {
		navigate('/products');
	}

	function addToCart(e){
		e.preventDefault();

		const item = {id: productId, name: name, price: price, stock: stock}

		if(stock === 0 ){
				Swal.fire({
					title: "Add item failed",
					icon: "error",
					text:"Not enough Stock."
				})
		}
		else{
		add(item,1);
		Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product added to Cart."
				}).then(() =>{
					navigate('/products');
				});
		}
	}

	useEffect(()=>{
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setFunkoNumber(data.funkoNumber);
			setImage(data.image);
			setStock(data.stock);
		})
	},[productId]);

	return(
		<Container className="mt-5">
			<Row className="mt-3 mb-3">
				<Col xs={12} md={{span:6, offset:3}}>
					<Card className="bgGray">
						<Card.Body className="text-center">

							<Card.Text><img className="imgThumbnail" src={image} /></Card.Text>

							<Card.Title>{name} #{funkoNumber}</Card.Title>

							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>

							<Card.Subtitle>Stock</Card.Subtitle>
							<Card.Text>{stock}</Card.Text>
							

							{ (user.id !== null)?
							<Fragment>	
							<Button 
							className="backBtn"
							onClick={navigateProducts}>
								Back
							</Button>
							
							&nbsp;&nbsp;&nbsp;

							<Button 
							className = "btnStd"
							onClick={(productId) => addToCart(productId)}>
								Add to Cart
							</Button>

							</Fragment>
							:
							<Link className='btn loginToPurhcaseBtn btn-block' to='/login'>Login to purchase</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)



}
