import { Row, Col, Card, Container } from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';

export default function Highlights(){

  return (
  	<Container className="d-flex highlightPhoto">
    <Carousel  fade>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 img-thumbnail"
          src="https://previews.dropbox.com/p/thumb/ABqW0khcZJg9zKFjh5w0htKwhhsKCh4hZ4YAbgk0miRRoNljkDpKUyC3o9QEBCoV8Xy9niiVcqi1VoHWQ1_EJGXiK3P8OGdNbozTqrq9la-QcExNUtYvcs0oafonf7Ey4ijZoZqvPtffy5RHA_XyjLOsCnxq3LjbATdUi8WIF0uK2ZauaaZveY7Yl1psv8WHiIUw2CajVfxDkovWigU0EozvVofwagsTfDVBlWN3pY4b0RlIzb9qwF_y8p8P0KWwSEufhoxs4MK-TEWcs_9AyUNEiL0OfUQv79waDQw-p5GdfxWH-9rO4PXFxW4TrIu8URdgG7IzoV7gyCVUKouzeao5XbtYWjzyBENNlJvRTiyP4uuoPNAFv6NBNPnjHRZNGJi6XrYWi2FmlPsGRN-GG5POP4VmSJ8_XP7YVN7tPqOjwJRTvtS-BCZeEJeYlhX0Gl0/p.jpeg"
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 img-thumbnail"
          src="https://previews.dropbox.com/p/thumb/ABqyY46YUU2oRYxxsvBBtDwRXGAmKIJM4SaACbr-uCmYKBDJgzTc_qfOrjTK6m5700Fm8h_BKW30NizR1refhzvLw0cH_nSe7fElRQT-J1hflij87CTY3aywTNsGZqPp7SzmL3gKkPcw25VsA8ttarUVNQH5jsDRV4KvYMYhQlQoztuENUNQRhMT8hVMNT2I1MtbhQs6tdXuMjWmT623GW23tMDTEksSnzOSlZQPL7kbYoKfyAIkxyHJ8MoIMg9IZbpvQL3WKBHnoszXXz4XaDjVNINNaEBe2zsaXaFYphpyEKHZ8PFag-ev0zJnEwoTfb4OGS2mFk-9Pb5rzW4p7j6XtziMcRDR4YXEFeboPouccg2j5sV5KQYZbQHTm1t8lPAIUzwT-hyPN_32g42E0LQPfrZq8JXxHaLam-b8wuejc_ItMupXkAZq-0UhMW5KYqY/p.jpeg"
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 img-thumbnail"
          src="https://previews.dropbox.com/p/thumb/ABre6pdUmETtD3vhMc1it_8niJqkd3a8e3FOBwZitncV8TPdlUw7MeY2Vp2Bv9q2SVx1J59JqA2dKIivGQn3fmm7q4DgCiu5hUHdMKO9OF-5d-yF5GZOooY1KN4rMLXRJEVR9Odn7jz6xppEDg7dzwFTE5Ym4EB552LTam5sKDAYSqGRUaT4SeJ_2qTTvFGokzPxnYMr4cH0w-fSqF0K8IiPpO24sUBExjBHwHebSdfcE-KLYnF4GXF6oekDkNJE118Jscb3N9pGzZdxvK3i8z7YNNq26DdmS0uIRzMMCadLrApLvcVF7nRtGOSY3LhIjuQrQoej2mfcWUzxIHyIqEpR1J5v1347XhkCOvDJ4EVCx22Exk_EWvXIyHm_P2e353Y9DLQIxpzFPtoa-J9fx30OMCGPeJOYpnMoDPeKKdlhnI__N1ivbmSHsE-2oogW0hA/p.jpeg"
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 img-thumbnail"
          src="https://previews.dropbox.com/p/thumb/ABrr0ZhKJWJsc-_WZx6AUGw9d5bpcfu6a3qdOyrihWXXhP4-4pvjSXmLREAB8y59U-pK7Gx4Fzu2XCTup9OCdCGahDK15Nz7DGnxZhFd3nmZcUouInELk76CX2jNO6GOg2vi6Oc6SB84nB9s_gXBoREH3iCoML_NS-fZf2xuVMYi-hdpRkURtxkUV6KyWqVvx92X0fUZ_maghTeEMN2xZBHBJaVu_Cr5oncqCiMEIjJZqBlvaR71IEIJxON7lKEm0O5F2RRR3XzQdnQl3PgEhL1ALiB6Q5_fL4VWfphgqF_DI2pndtiYjnfyy1XO-JubY9IS036rs_KN9knuDZ2C0AZrFamgw0nGTDF5TZegeTwMTqVYKFpfpZDNnPHuqGxNjYhjK7ABMAi4hxmn-eqc2gaDnzqPMZkapwUoLfSUtkDvF8lacQZMrbEuitiaEQeJlh4/p.jpeg"
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 img-thumbnail"
          src="https://previews.dropbox.com/p/thumb/ABqjc68eikuk_mImdgJ06jKvQdvIdlERB_sywAtPvfC1vzk4fgP39uRkhDHehWhhPmND9SoYgByVcc7_0pX1dlx7lKS1-ngL9IQXpvnVK6CJM1AmLaWVMr8pVBxLjaOsGoBQ8qFqQ03YrzmOkZ8XnKSez76dnxdZ9obYg3b0x1e4pohrl_oXCll0dqgou4SZi8_jiXmzxeUg6P75xGv-l3qKO25oFKbkzEOmM-xQnaZsXY6r01HRTqwnm9mUlU2XWI4MssVesyZli377Nnq5YUzWSRIommYAZGXxS48hSf_JLIZjyo3-1ga1O3ddVHVUE6l52HFoDbsu_MD-lsc9Secpm03wimak1l-EH5OWiwQdvjcslFPrKlMxHd7h6cW72g3qqIuEmyXrd0WAp3T5CIrArWHVFmkSR1POGe3d8SHl_9yo65xXaEMO-aCYfiOv-04/p.jpeg"
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 img-thumbnail"
          src="https://previews.dropbox.com/p/thumb/ABpPG5msUS9Af9K1cBARa8_dp04_AzXoKyTZwLIHQAr_GBwK2XB9y9ES-OdOBhAwvWUM2d1cqnHf8BJk8b5awejeo8bSVtdtWFOtW-EmEoMZpHw2nAowtsxhWok-XffwwHTnHLMCKuU51pesdBGwTKD_KYr5Px2umOE9tGyAtheYhvduttAx1SFcITdcf8BNQnG2Exva_XFLbEWkga8HcDouYfDUVCL-t1BvU9iKO7O8pwR1ffOGWVbjHCBpr-8-fIV3axOR2J0nEwJLyk5SbBaj8-xVaR5Xw0e2uJv8l1AlFZv5pufhbb_Bxjuk4EsNN9GFK0-f0D54SVOSspuVIq2meiYJtBzOOwoxtPkFh9VBY9NHdBL3a46UHSMz5a7BnjlPT-oXuqxTHQEt7N65CCbbHDA2sMqKUub4tzQMxdTFL1wWDY_1W82a38BJOSDcaPY/p.jpeg"
        />
      </Carousel.Item>	
    </Carousel>
    </Container>
  );
}
